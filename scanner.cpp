/*
 * A sample lexical analyzer
 * Authors : Philip Karunakaran, Immanuel Rajkumar; Madhanagopal, Praveenkumar; Thomas, Ron
 * input  : inputProgram <filename>
 * output : tokens.out
 */

#include<iostream>
#include<cstdio>
#include<string>
#include<fstream>
#include<vector>
#include<cstdlib>

using namespace std;

/* Structure to store the tokens identified by the scanner */
struct Token {
    string lexeme;
    string token_type;
    string token_value;
};

/* Regex manipulations */
class RegexDriver
{

public:

    int lcurlyMatch( string s ) {
        if( s == "{" ) return 1;
        return 0;
    }
    int rcurlyMatch( string s ) {
        if( s == "}" ) return 1;
        return 0;
    }


    int lbraceMatch( string s ) {
        if( s == "(" ) return 1;
        return 0;
    }

    int rbraceMatch( string s ) {
        if( s == ")" ) return 1;
        return 0;
    }

    int commaMatch( string s ) {
        if( s == "," ) return 1;
        return 0;
    }

    int dotMatch( string s ) {
        if( s == "." ) return 1;
        return 0;
    }

    int identifierMatch( string s ) {
        return 1;
    }

    int whitespaceMatch( string s ) {
        int n=s.size();
        int state=0;
        for( int i=0; i<n; i++ ) {
            if( s[i]==' ' || s[i]=='\n' || s[i]=='\t') {
                if( state==0 ) state=1;
            } else {
                if( state==1 ) state=2;
                break;
            }
        }
        if( state==1 ) return 1;
        return 0;
    }

    int numberMatch( string s ) {
        int n=s.size();
        int state=0;
        for( int i=0; i<n; i++ ) {
            int num = s[i]-'0';
            if( num >=0 && num<=9 )
                state=1;
            else {
                state=0;
                break;
            }
        }
        if( state== 1 ) return 1;
        return 0;
    }

    int testRegex( string curLexeme ) {
        //cout<<"\n Lexeme Under Test : "<<curLexeme;
        //Order will be based on the priority
        if( lcurlyMatch(curLexeme) ) 
            return 1;
        else if( rcurlyMatch(curLexeme) ) 
            return 2;
        else if( lbraceMatch(curLexeme) ) 
            return 3;
        else if( rbraceMatch(curLexeme) ) 
            return 4;
        else if( commaMatch(curLexeme) ) 
            return 5;
        else if( dotMatch(curLexeme) ) 
            return 6;
        else if( numberMatch(curLexeme) ) 
            return 7;
        else if( whitespaceMatch(curLexeme) ) 
            return 8;
        //	else if( identifierMatch(curLexeme) ) { cout<<"\nIdentfier Found!"; return 1; }
        return 0;
    }
};

/*
 * Scanner - Reads an input (source) file; Identifies the lexemes; Creates a symbol table and stores new lexemes
 * Input Source File : inputProgram
 * Stream of tokens are stored in the output file - token.out
 */
class Scanner
{
private:
    string inputFile;
    string outputFile;
    string curLexeme;
    int fwdPtr;
    int lexemeBegin;
    char unit;
    bool reset;
    vector<Token> tokenList;
    RegexDriver regCheck;

public:
    Scanner() {
        inputFile = "test";
        //inputFile = "inputProgram";
        outputFile = "tokens.out";
        fwdPtr = 0;
        lexemeBegin = 0;
        curLexeme = "";
        reset=false;
    }

    /* Print the stream of tokens identified after scanning */
    void printOutput(void) {
        cout<<"\nPrinting Output\n";
    }

    /* Scan input source file; Identify lexemes */
    int scanInput() {
        ifstream ifile(inputFile.c_str());
        if( ifile.is_open() ) {
            ifile.seekg(0,ios::beg);
            int type = 0, typePrev=0;
            while(1) {
                if( !reset ) {
                    if( !ifile.get(unit) )
                        break;
                    curLexeme += unit;
                }

                reset=false;
                fwdPtr++ ;
                type = regCheck.testRegex(curLexeme);
                if (!type) {
                    curLexeme.erase(curLexeme.size()-1);
                    cout<<"\nLexeme Found : ["<<curLexeme<<"] ";

                    switch(typePrev) {
                    case 1: {
                        cout<<" Type : LCURLY ";
                        break;
                    }
                    case 2: {
                        cout<<" Type : RCURLY ";
                        break;
                    }
                    case 3: {
                        cout<<" Type : LBRACE ";
                        break;
                    }
                    case 4: {
                        cout<<" Type : RBRACE ";
                        break;
                    }
                    case 5: {
                        cout<<" Type : COMMA ";
                        break;
                    }
                    case 6: {
                        cout<<" Type : DOT ";
                        break;
                    }
                    case 7: {
                        cout<<" Type : NUMBER ";
			if( curLexeme[0] == '0' && curLexeme.size()!=1 )
			cout<<" - Error in Number Format";
                        //Can use return inside the if above if the program needs to terminate on encountering an error
			break;
                    }
                    case 8: {
                        cout<<" Type : WHITESPACE ";
                        break;
                    }
                    case 9: {
                        cout<<" Type : IDENTIFIER ";
                        break;
                    }
                    case 10: {
                        cout<<" Type : TYPE REFERENCE ";
                        break;
                    }
                    default: {
                        cout<<" Type : Unknown - ERROR ";
                    }
                    }

                    curLexeme = unit; //reset the lexeme
                    reset = true;
                    lexemeBegin = fwdPtr-1;
                    fwdPtr++;
                    //Ideally, lexemeBegin and fwdPtr will be used to get the lexeme, but we get the lexeme from the stored curLexeme variable
                } else {
                    typePrev = type;
                }
            }

            ifile.close();
            return 1;
        }
        return 0;
    }

};


int main(void)
{
    Scanner s;
    if( !s.scanInput() )
        cout<<"\nError Scanning the Program\n";
//    else
//        s.printOutput();

    return 0;
}
