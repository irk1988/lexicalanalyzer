/*
 * A sample lexical analyzer
 * Authors : Philip Karunakaran, Immanuel Rajkumar
 * input  : inputProgram <filename>
 */

#include<iostream>
#include<cstdio>
#include<string>
#include<fstream>
#include<vector>
#include<regex>

using namespace std;

/* Structure to store the tokens identified by the scanner */
struct Token {
    string lexeme;
    string token_type;
    string token_value;
};

/* Regex manipulations */
class RegexDriver
{

private:
	regex identifier;
	regex number;
	regex lcurly;
	regex rcurly;
	regex comma;

public:
    RegexDriver() {
	identifier = regex("([A-Z])([a-z]*)");
	number = regex("([0-9])*");	
	lcurly = regex("\\{");
	rcurly = regex("\\}");
	comma = regex(",");
    }	
    
    int testRegex( string curLexeme ) {
	cout<<"\nlexeme Under Test inside RegexDriver : "<<curLexeme;
	if( regex_match( curLexeme, identifier ) ) {
		cout<<"\nIdentfier Found!";
		return 1;
	}
	else if( regex_match( curLexeme, number ) ) {
		cout<<"\nNumber Found!";
		return 1;
	}
	else if( regex_match( curLexeme, lcurly ) ) {
		cout<<"\nlcurly Found!";
		return 1;
	}
	else if( regex_match( curLexeme, rcurly ) ) {
		cout<<"\nrcurly Found!";
		return 1;
	}
	else if( regex_match( curLexeme, comma ) ) {
		cout<<"\ncomma Found!";
		return 1;
	}
	
	return 0;
    }

};

/*
 * Scanner - Reads an input (source) file; Identifies the lexemes; Creates a symbol table and stores new lexemes
 * Input Source File : inputProgram
 * Stream of tokens are stored in the output file - token.out
 */
class Scanner
{
private:
    string inputFile;
    string outputFile;
    string curLexeme;
    int fwdPtr;
    int lexemeBegin;
    char unit;
    vector<Token> tokenList;
    RegexDriver regCheck;

public:
    Scanner() {
        inputFile = "test";
	//inputFile = "inputProgram";
        outputFile = "tokens.out";
        fwdPtr = 0;
        lexemeBegin = 0;
	curLexeme = "";
    }

    /* Print the stream of tokens identified after scanning */
    void printOutput(void) {
        cout<<"\nPrinting Output\n";

    }

    /* Scan input source file; Identify lexemes */
    int scanInput() {
        ifstream ifile(inputFile.c_str());
        if( ifile.is_open() ) {
            ifile.seekg(0,ios::beg);
            while( ifile.get(unit) ) {
		curLexeme += unit;
		fwdPtr++ ;
		if ( !regCheck.testRegex(curLexeme) ) {
			cout<<"\n\nLexeme Found : "<<curLexeme<<endl<<endl;
			curLexeme = ""; //reset the lexeme
			lexemeBegin = fwdPtr-1;
			fwdPtr++;
			//Ideally, lexemeBegin and fwdPtr will be used to get the lexeme, but we get the lexeme from the stored curLexeme variable
		}
		else {
			//cout<<"\nMatching in Progress : "<<curLexeme;
		}
            }
            ifile.close();
            return 1;
	}
	return 0;
    }

};


int main(void)
{
    
    Scanner s;
    if( !s.scanInput() )
        cout<<"\nError Scanning the Program\n";
    else
        s.printOutput();
    
/*
    RegexDriver rd;
    string r = "a";
    string input = "aaaaa";
    rd.test(input, r);
*/
    printf("\nTest Sample\n");
    return 0;
}
